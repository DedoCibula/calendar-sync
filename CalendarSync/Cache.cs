﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CalendarSync
{
    public static class Cache
    {
        private static readonly ConcurrentDictionary<Type, object> CachedObjects = new ConcurrentDictionary<Type, object>();

        public static bool Contains<T>()
        {
            return CachedObjects.ContainsKey(typeof (T));
        }

        public static T Get<T>() where T : class
        {
            object item;
            CachedObjects.TryGetValue(typeof (T), out item);
            return item as T;
        }

        public static T GetOrAdd<T>() where T : class, new()
        {
            if (!Contains<T>())
                CachedObjects.TryAdd(typeof (T), new T());
            return Get<T>();
        }

        public static void StoreOrUpdate<T>(T item) where T : class
        {
            var type = typeof (T);
            if (!Contains<T>())
                CachedObjects.TryAdd(type, item);
            else
                CachedObjects.TryUpdate(type, item, Get<T>());
        }

        public static void Reset()
        {
            CachedObjects.Clear();
        }
    }
}
