﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Outlook;

namespace CalendarSync
{
    public class CalendarService : IDisposable
    {
        private Application _context;

        public HashSet<AppointmentItem> Added { get; private set; }
        public HashSet<AppointmentItem> Modified { get; private set; }
        public HashSet<AppointmentItem> Deleted { get; private set; }

        public CalendarService(Application context)
        {
            _context = context;
            Added = new HashSet<AppointmentItem>();
            Modified = new HashSet<AppointmentItem>();
            Deleted = new HashSet<AppointmentItem>();
        }

        public async Task<bool> RetrieveLatestChangesAsync()
        {
            // TODO cache invalidation
            return await Task.Factory.StartNew(() =>
            {
                var cachedAppointments = Cache.GetOrAdd<Dictionary<string, AppointmentItem>>();
                var appointments = _context.Session.GetDefaultFolder(OlDefaultFolders.olFolderCalendar).Items;
                var cachedCount = cachedAppointments.Count;
                var appCount = appointments.Count;
                ClearPreviousAppointments();
                foreach (AppointmentItem appointment in appointments)
                {
                    var id = appointment.GlobalAppointmentID;
                    if (!cachedAppointments.ContainsKey(id))
                    {
                        if (cachedCount <= appCount)
                        {
                            Added.Add(appointment);
                            cachedAppointments[id] = appointment;
                        }
                        else
                        {
                            Deleted.Add(appointment);
                        }
                    }
                    else
                    {
                        if (appointment.LastModificationTime > cachedAppointments[id].LastModificationTime)
                        {
                            Modified.Add(appointment);
                            cachedAppointments[id] = appointment;
                        }
                    }
                }
                return Added.Count + Modified.Count + Deleted.Count == 0;
            });
        }

        public async Task CreateOrModifyAppointmentAsync<T>(AbstractMapper<T, AppointmentItem> mapper, T template) where T : class
        {
            if (mapper == null || template == null)
                throw new ArgumentNullException(mapper == null ? "mapper" : "template");

            AppointmentItem appointment = await FindInCacheAsync(mapper, template) ?? _context.CreateItem(OlItemType.olAppointmentItem);
            // TODO exception
            mapper.Map(from: template, to: appointment);
            appointment.Save();
            Cache.Get<Dictionary<string, AppointmentItem>>()[appointment.GlobalAppointmentID] = appointment;
        }

        public async Task DeleteAppointmentAsync<T>(AbstractMapper<T, AppointmentItem> mapper, T template) where T : class
        {
            if (mapper == null || template == null)
                throw new ArgumentNullException(mapper == null ? "mapper" : "template");

            var appointment = await FindInCacheAsync(mapper, template);
            if (appointment != null)
            {
                appointment.Delete();
                Cache.Get<Dictionary<string, AppointmentItem>>().Remove(appointment.GlobalAppointmentID);
            }
        }

        public void Dispose()
        {
            _context = null;
        }
        
        private async Task<AppointmentItem> FindInCacheAsync<T>(AbstractMapper<T, AppointmentItem> mapper, T template)
        {
            return await Task.Factory.StartNew(() =>
            {
                // TODO exception
                var appointments = Cache.Get<Dictionary<string, AppointmentItem>>();
                foreach (var item in appointments.Values)
                    if (mapper.AreEqual(template, item))
                        return item;
                return null;
            });
        }

        private void ClearPreviousAppointments()
        {
            Added.Clear();
            Modified.Clear();
            Deleted.Clear();
        }
    }
}
