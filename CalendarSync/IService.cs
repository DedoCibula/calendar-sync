﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Outlook;

namespace CalendarSync
{
    public interface IService<T> : IDisposable where T : ItemEvents_10_Event
    {
        Task<bool> RetrieveLatestChangesAsync();

        IEnumerable<object> GetAdded();

        IEnumerable<object> GetModified();

        IEnumerable<object> GetDeleted();

        AbstractMapper<object, T> GetDefaultMapper();
            
        Task CreateOrModifyAsync(AbstractMapper<object, T> mapper, T template);

        Task DeleteAsync(AbstractMapper<object, T> mapper, T template);
    }
}
