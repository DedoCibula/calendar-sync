﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarSync
{
    internal class SettingsProvider
    {
        public static readonly Settings Google = new Settings();

        public class Settings
        {
            private readonly Properties.Settings _settings = (Properties.Settings)SettingsBase.Synchronized(new Properties.Settings());

            public void Flush<T>(string key, T value)
            {
                if (string.IsNullOrEmpty(key))
                    throw new ArgumentException(@"key cannot be null or empty.", "key");
                _settings[key] = value;
            }

            public void Delete(string key)
            {
                if (string.IsNullOrEmpty(key))
                    throw new ArgumentException(@"key cannot be null or empty.", "key");
                _settings.Properties.Remove(key);
            }

            public T Get<T>(string key)
            {
                if (string.IsNullOrEmpty(key))
                    throw new ArgumentException(@"key cannot be null or empty.", "key");
                return (T)_settings[key];
            }

            public void Clear()
            {
                _settings.Properties.Clear();
            }

            public void Commit()
            {
                _settings.Save();
            }
        }

        public static void Flush<T>(string key, T value)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentException(@"key cannot be null or empty.", "key");
            Properties.Settings.Default[key] = value;
        }

        public static void Delete(string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentException(@"key cannot be null or empty.", "key");
            Properties.Settings.Default.Properties.Remove(key);
        }

        public static T Get<T>(string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentException(@"key cannot be null or empty.", "key");
            return (T) Properties.Settings.Default[key];
        }

        public static void Clear()
        {
            Properties.Settings.Default.Properties.Clear();
        }

        public static void Commit()
        {
            Properties.Settings.Default.Save();
        }
    }
}
