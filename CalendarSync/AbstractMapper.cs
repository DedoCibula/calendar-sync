﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Outlook;

namespace CalendarSync
{
    public abstract class AbstractMapper<T1, T2>
    {
        public abstract void Map(T1 from, T2 to);

        public abstract void InverseMap(T2 from, T1 to);

        public abstract bool AreEqual(T1 item1, T2 item2);
    }
}
