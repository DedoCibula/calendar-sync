﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Calendar.v3.Data;
using Microsoft.Office.Interop.Outlook;

namespace CalendarSync.Google
{
    public class GoogleEventMapper : AbstractMapper<object, AppointmentItem>
    {
        public override void Map(object from, AppointmentItem to)
        {
            if (from == null)
                throw new ArgumentNullException("from");
            if (to == null)
                throw new ArgumentNullException("to");
            
            var googleEvent = (Event) from;
            to.Subject = googleEvent.Summary;
            to.Start = googleEvent.Start.DateTime.GetValueOrDefault();
            to.End = googleEvent.End.DateTime.GetValueOrDefault();
            to.Location = googleEvent.Location;
        }

        public override void InverseMap(AppointmentItem from, object to)
        {
            if (from == null)
                throw new ArgumentNullException("from");
            if (to == null)
                throw new ArgumentNullException("to");
            
            var googleEvent = (Event) to;
            googleEvent.Summary = from.Subject;
            googleEvent.Start = new EventDateTime {DateTime = from.Start};
            googleEvent.End = new EventDateTime() {DateTime = from.End};
            googleEvent.Location = from.Location;
        }

        public override bool AreEqual(object item1, AppointmentItem item2)
        {
            if (item1 == null || item2 == null || !(item1 is Event))
                return false;

            var googleEvent = (Event) item1;
            return googleEvent.Summary == item2.Subject
                   && googleEvent.Start.DateTime.GetValueOrDefault() == item2.Start
                   && googleEvent.End.DateTime.GetValueOrDefault() == item2.End;
        }
    }
}
