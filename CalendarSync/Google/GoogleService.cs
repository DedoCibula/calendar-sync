﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.New;
using Google.Apis.Services;
using Microsoft.Office.Interop.Outlook;
using G = Google.Apis.Calendar.v3;

namespace CalendarSync.Google
{
    public class GoogleService : IService<AppointmentItem>
    {
        private G.CalendarService _calendarService;
        private const String ClientSecret = "client_secret.json";
        private string _defaultCalendar;
        private GoogleEventMapper _defaultEventMapper;

        private HashSet<Event> _addedEvents;
        private HashSet<Event> _modifiedEvents;
        private HashSet<Event> _deletedEvents;

        public GoogleService()
        {
            _addedEvents = new HashSet<Event>();
            _modifiedEvents = new HashSet<Event>();
            _deletedEvents = new HashSet<Event>();
            InitializeCalendarService();
        }

        public async Task<bool> RetrieveLatestChangesAsync()
        {
            return await Task.Factory.StartNew(() =>
            {
                var calendarId = GetDefaultCalendar();
                var eventTask = Task.Factory.StartNew<Events>(_calendarService.Events.List(calendarId).Execute);
                var cachedEvents = Cache.GetOrAdd<Dictionary<string, Event>>();
                var cachedCount = cachedEvents.Count;
                var events = eventTask.Result.Items;
                var eventCount = events.Count;
                ClearPreviousEvents();
                foreach (var googleEvent in events)
                {
                    var id = googleEvent.Id;
                    if (!cachedEvents.ContainsKey(id))
                    {
                        if (cachedCount <= eventCount)
                        {
                            _addedEvents.Add(googleEvent);
                            cachedEvents[id] = googleEvent;
                        }
                        else
                        {
                            _deletedEvents.Add(googleEvent);
                        }
                    }
                    else
                    {
                        if (googleEvent.Updated > cachedEvents[id].Updated)
                        {
                            _modifiedEvents.Add(googleEvent);
                            cachedEvents[id] = googleEvent;
                        }
                    }
                }
                return false;
            });
        }

        public IEnumerable<object> GetAdded()
        {
            return _addedEvents;
        }

        public IEnumerable<object> GetModified()
        {
            return _modifiedEvents;
        }

        public IEnumerable<object> GetDeleted()
        {
            return _deletedEvents;
        }

        public AbstractMapper<object, AppointmentItem> GetDefaultMapper()
        {
            _defaultEventMapper = _defaultEventMapper ?? new GoogleEventMapper();
            return _defaultEventMapper;
        }

        public async Task CreateOrModifyAsync(AbstractMapper<object, AppointmentItem> mapper, AppointmentItem template)
        {
            await Task.Factory.StartNew(() =>
            {
                if (mapper == null || template == null)
                    throw new ArgumentNullException(mapper == null ? "mapper" : "template");

                var googleEvent = FindInCache(mapper, template);
                if (googleEvent != null)
                {
                    mapper.InverseMap(@from: template, to: googleEvent);
                    // TODO exception
                    googleEvent = _calendarService.Events.Patch(googleEvent, GetDefaultCalendar(), googleEvent.Id).Execute();
                }
                else
                {
                    googleEvent = new Event();
                    mapper.InverseMap(@from: template, to: googleEvent);
                    // TODO exception
                    googleEvent = _calendarService.Events.Insert(googleEvent, GetDefaultCalendar()).Execute();
                }
                Cache.Get<Dictionary<string, Event>>()[googleEvent.Id] = googleEvent;
            });
        }

        public async Task DeleteAsync(AbstractMapper<object, AppointmentItem> mapper, AppointmentItem template)
        {
            await Task.Factory.StartNew(() =>
            {
                if (mapper == null || template == null)
                    throw new ArgumentNullException(mapper == null ? "mapper" : "template");

                var googleEvent = FindInCache(mapper, template);
                if (googleEvent != null)
                {
                    var eventId = _calendarService.Events.Delete(GetDefaultCalendar(), googleEvent.Id).Execute();
                    Cache.Get<Dictionary<string, Event>>().Remove(eventId);
                }
            });
        }

        public void Dispose()
        {
            _calendarService.Dispose();
        }

        private void InitializeCalendarService()
        {
            UserCredential credentials;
            if (Cache.Contains<UserCredential>())
                credentials = Cache.Get<UserCredential>();
            else
            {
                using (var stream = new FileStream(ClientSecret, FileMode.Open))
                {
                    credentials = CustomWebAuthorizationBroker.AuthorizeAsync(
                        stream,
                        new[] { G.CalendarService.Scope.Calendar },
                        "user",
                        CancellationToken.None).Result;
                    Cache.StoreOrUpdate(credentials);
                }
            }

            _calendarService = new G.CalendarService(new BaseClientService.Initializer { HttpClientInitializer = credentials });
        }

        private string GetDefaultCalendar()
        {
            // TODO no calendar, cache
            _defaultCalendar =
                _defaultCalendar ??
                _calendarService.CalendarList
                                .List()
                                .Execute()
                                .Items
                                .First()
                                .Id;
            return _defaultCalendar;
        }

        private Event FindInCache(AbstractMapper<object, AppointmentItem> mapper, AppointmentItem template)
        {
            // TODO exception
            var events = Cache.Get<Dictionary<string, Event>>();
            foreach (var item in events.Values)
                if (mapper.AreEqual(item, template))
                    return item;
            return null;
        }

        private void ClearPreviousEvents()
        {
            _addedEvents.Clear();
            _modifiedEvents.Clear();
            _deletedEvents.Clear();
        }
    }
}
