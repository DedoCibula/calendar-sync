﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.New;

namespace CalendarSync.Google
{
    public class GoogleDataProvider : IGoogleDataProvider
    {
        public Action Store<T>(string key, T value)
        {
            var googleSettings = SettingsProvider.Google;
            return () =>
            {
                googleSettings.Flush(key, value);
                googleSettings.Commit();
            };
        }

        public Action Delete(string key)
        {
            var googleSettings = SettingsProvider.Google;
            return () =>
            {
                googleSettings.Delete(key);
                googleSettings.Commit();
            };
        }

        public Func<T> Get<T>(string key)
        {
            return () => SettingsProvider.Google.Get<T>(key);
        }

        public Action Clean()
        {
            var googleSettings = SettingsProvider.Google;
            return () =>
            {
                googleSettings.Clear();
                googleSettings.Commit();
            };
        }
    }
}
