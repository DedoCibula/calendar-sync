﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalendarSync.Outlook;
using Microsoft.Office.Interop.Outlook;

namespace CalendarSync
{
    public class Synchronizer
    {
        private readonly CalendarService _calendarService;
        private readonly IService<AppointmentItem> _service;

        public Synchronizer(CalendarService calendarService, IService<AppointmentItem> service)
        {
            // TODO temporary solution
            _calendarService = calendarService;
            _service = service;
        }

        public async void Synchronize()
        {
            var calendarChanges = _calendarService.RetrieveLatestChangesAsync();
            var providerChanges = _service.RetrieveLatestChangesAsync();
            Task syncOnProvider = null;
            Task syncOnLocal = null;
            if (await calendarChanges)
            {
                syncOnProvider = SynchronizeOnProviderAsync();
            }
            if (await providerChanges)
            {
                syncOnLocal = SynchronizeOnLocalAsync();
            }
            await Task.WhenAll(syncOnLocal, syncOnProvider);
        }

        private async Task SynchronizeOnProviderAsync()
        {
            // TODO refactor
            var mapper = _service.GetDefaultMapper();
            await Task.WhenAll(_calendarService.Added.Select(appointmentItem => _service.CreateOrModifyAsync(mapper, appointmentItem))
                        .Union(_calendarService.Modified.Select(appointmentItem => _service.CreateOrModifyAsync(mapper, appointmentItem)))
                        .Union(_calendarService.Deleted.Select(appointmentItem => _service.DeleteAsync(mapper, appointmentItem))));
        }

        private async Task SynchronizeOnLocalAsync()
        {
            await new Task(async () =>
            {
                // TODO refactor
                var mapper = _service.GetDefaultMapper();
                foreach (var item in _service.GetAdded())
                    await _calendarService.CreateOrModifyAppointmentAsync(mapper, item);
                foreach (var item in _service.GetModified())
                    await _calendarService.CreateOrModifyAppointmentAsync(mapper, item);
                foreach (var item in _service.GetDeleted())
                    await _calendarService.DeleteAppointmentAsync(mapper, item);
            });
        }
    }
}
