﻿using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Requests;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Logging;

namespace Google.Apis.New
{
    public class CustomServerCodeReceiver : ICodeReceiver
    {
        private static readonly ILogger Logger = ApplicationContext.Logger.ForType<CustomServerCodeReceiver>();

        private readonly WebBrowser _webBrowser;
        private string _redirectUri;

        public string RedirectUri
        {
            get
            {
                _redirectUri = _redirectUri ?? string.Format("http://localhost:{0}/authorize/", GetRandomUnusedPort());
                return _redirectUri;
            }
        }

        public CustomServerCodeReceiver(WebBrowser webBrowser = null)
        {
            _webBrowser = webBrowser;
        }

        public async Task<AuthorizationCodeResponseUrl> ReceiveCodeAsync(AuthorizationCodeRequestUrl url, CancellationToken taskCancellationToken)
        {
            var authorizationUrl = url.Build().ToString();
            AuthorizationCodeResponseUrl authorizationCodeResponseUrl;
            using (var source = new HttpListener())
            {
                source.Prefixes.Add(RedirectUri);
                try
                {
                    source.Start();
                    Logger.Debug("Open a browser with \"{0}\" URL", authorizationUrl);
                    if (_webBrowser != null)
                        _webBrowser.Navigate(authorizationUrl);
                    else
                        Process.Start(authorizationUrl);
                    var context = await AwaitExtensions.ConfigureAwait(AsyncPlatformExtensions.GetContextAsync(source), false);
                    var coll = context.Request.QueryString;
                    using (var streamWriter = new StreamWriter(context.Response.OutputStream))
                    {
                        streamWriter.WriteLine("<html>\r\n  <head><title>OAuth 2.0 Authentication Token Received</title></head>\r\n  <body>\r\n    Received verification code. Closing...\r\n    <script type='text/javascript'>\r\n      window.setTimeout(function() {\r\n          window.open('', '_self', ''); \r\n          window.close(); \r\n        }, 1000);\r\n      if (window.opener) { window.opener.checkToken(); }\r\n    </script>\r\n  </body>\r\n</html>");
                        streamWriter.Flush();
                    }
                    context.Response.OutputStream.Close();
                    authorizationCodeResponseUrl = new AuthorizationCodeResponseUrl(coll.AllKeys.ToDictionary(k => k, k => coll[k]));
                }
                finally
                {
                    source.Close();
                }
            }
            return authorizationCodeResponseUrl;
        }

        private static int GetRandomUnusedPort()
        {
            var tcpListener = new TcpListener(IPAddress.Loopback, 0);
            try
            {
                tcpListener.Start();
                return ((IPEndPoint)tcpListener.LocalEndpoint).Port;
            }
            finally
            {
                tcpListener.Stop();
            }
        }
    }
}

