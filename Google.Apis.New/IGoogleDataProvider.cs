﻿using System;

namespace Google.Apis.New
{
    public interface IGoogleDataProvider
    {
        Action Store<T>(string key, T value);

        Action Delete(string key);

        Func<T> Get<T>(string key);

        Action Clean();
    }
}