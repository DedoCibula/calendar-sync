﻿using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Util.Store;

namespace Google.Apis.New
{
    public static class CustomWebAuthorizationBroker
    {
        public static string Folder = "Google.Apis.Auth";

        public static async Task<UserCredential> AuthorizeAsync(ClientSecrets clientSecrets, IEnumerable<string> scopes, string user, CancellationToken taskCancellationToken, WebBrowser browser = null, IDataStore dataStore = null)
        {
            var initializer = new GoogleAuthorizationCodeFlow.Initializer { ClientSecrets = clientSecrets };
            return await AwaitExtensions.ConfigureAwait(AuthorizeAsyncCore(initializer, scopes, user, taskCancellationToken, browser, dataStore), false);
        }

        public static async Task<UserCredential> AuthorizeAsync(Stream clientSecretsStream, IEnumerable<string> scopes, string user, CancellationToken taskCancellationToken, WebBrowser browser = null, IDataStore dataStore = null)
        {
            var initializer = new GoogleAuthorizationCodeFlow.Initializer { ClientSecretsStream = clientSecretsStream };
            return await AwaitExtensions.ConfigureAwait(AuthorizeAsyncCore(initializer, scopes, user, taskCancellationToken, browser, dataStore), false);
        }

        public static async Task ReauthorizeAsync(UserCredential userCredential, CancellationToken taskCancellationToken, WebBrowser browser = null)
        {
            UserCredential newUserCredential = await AwaitExtensions.ConfigureAwait(new AuthorizationCodeInstalledApp(userCredential.Flow, new CustomServerCodeReceiver(browser)).AuthorizeAsync(userCredential.UderId, taskCancellationToken), false);
            userCredential.Token = newUserCredential.Token;
        }

        private static async Task<UserCredential> AuthorizeAsyncCore(GoogleAuthorizationCodeFlow.Initializer initializer, IEnumerable<string> scopes, string user, CancellationToken taskCancellationToken, WebBrowser browser, IDataStore dataStore = null)
        {
            initializer.Scopes = scopes;
            initializer.DataStore = dataStore ?? new FileDataStore(Folder);
            var flow = new GoogleAuthorizationCodeFlow(initializer);
            return await AwaitExtensions.ConfigureAwait(new AuthorizationCodeInstalledApp(flow, new CustomServerCodeReceiver(browser)).AuthorizeAsync(user, taskCancellationToken), false);
        }
    }
}
