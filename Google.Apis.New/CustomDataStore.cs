﻿using System;
using System.Threading.Tasks;
using Google.Apis.Util.Store;

namespace Google.Apis.New
{
    public class CustomDataStore : IDataStore
    {
        private readonly IGoogleDataProvider _provider;

        public CustomDataStore(IGoogleDataProvider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            _provider = provider;
        }

        public Task StoreAsync<T>(string key, T value)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentException(@"key cannot be null or empty.", "key");
            return Task.Factory.StartNew(_provider.Store(key, value));
        }

        public Task DeleteAsync<T>(string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentException(@"key cannot be null or empty.", "key");
            return Task.Factory.StartNew(_provider.Delete(key));
        }

        public Task<T> GetAsync<T>(string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentException(@"key cannot be null or empty.", "key");
            return Task.Factory.StartNew(_provider.Get<T>(key));
        }

        public Task ClearAsync()
        {
            return Task.Factory.StartNew(_provider.Clean());
        }
    }
}
